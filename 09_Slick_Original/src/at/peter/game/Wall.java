package at.peter.game;

import org.newdawn.slick.Color;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Wall implements Actor {

	private String type;
	private double x, y, width, height;
	private Shape shape;
	private Image brickImage;
	private Image woodImage;

	public Wall(String type, double x, double y, double width, double height) {
		super();
		this.type = type;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		this.shape = new Rectangle((int) this.x, (int) this.y, (int) this.width, (int) this.height);

		try {
			this.brickImage = new Image("images/brick2.png");
			this.woodImage = new Image("images/wood.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub

		this.shape.setX((int) this.x);
		this.shape.setY((int) this.y);
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		if (this.type == "brick") {
			graphics.setColor(Color.darkGray);
			for (int i = 1; i <= 10; i++) {
				graphics.fillRect((int) this.x + i, (int) this.y + i, Game.TILESIZE, Game.TILESIZE);
			}
			this.brickImage.draw((int) this.x, (int) this.y, (int)Game.TILESIZE, (int)Game.TILESIZE);
		} else if (this.type == "wood") {
			graphics.setColor(Color.darkGray);
			for (int i = 1; i <= 10; i++) {
				graphics.fillRect((int) this.x + i, (int) this.y + i, Game.TILESIZE, Game.TILESIZE);
			}
			this.woodImage.draw((int) this.x, (int) this.y);
		}

		// Hitbox
		if (Game.SHOWHITBOX == true) {
			graphics.setColor(Color.orange);
			graphics.draw(this.shape);
			graphics.setColor(Color.white);
		}
	}

	public void setX(double x) {
		this.x = x;
	}

	public String getType() {
		return type;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public Shape getShape() {
		return shape;
	}

}
