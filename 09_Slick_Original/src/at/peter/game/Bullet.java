package at.peter.game;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Bullet implements Actor {
	private int number;
	private double x, y, speed;
	private Game.Direction direction;
	private Image blueBullet, redBullet;
	private Shape shape;

	public Bullet(int number, double x, double y, double speed, Game.Direction direction) {
		super();
		this.number = number;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.direction = direction;

		try {
			this.blueBullet = new Image("images/bluebullet.png");
			this.redBullet = new Image("images/redbullet.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.shape = new Circle((float) this.x, (float) this.y, Game.TILESIZE / 5);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if (this.direction == Game.Direction.UP) {
			this.y -= delta * this.speed;
		}
		if (this.direction == Game.Direction.DOWN) {
			this.y += delta * this.speed;
		}
		if (this.direction == Game.Direction.LEFT) {
			this.x -= delta * this.speed;
		}
		if (this.direction == Game.Direction.RIGHT) {
			this.x += delta * this.speed;
		}

		this.shape.setX((int) this.x + 9);
		this.shape.setY((int) this.y + 10);

		// Detect if Bullet hits other Player
		if (this.shape.intersects(Game.player1.getShape()) && this.number == 2) {
			Game.player1.setHealth(Game.player1.getHealth() - 25);
			this.x = 1000;
		}
		if (this.shape.intersects(Game.player2.getShape()) && this.number == 1) {
			Game.player2.setHealth(Game.player2.getHealth() - 25);
			this.x = 1000;
		}

		// Detect if Bullet hits Wall
		for (Wall w : Game.player1.walls) {
			if (this.shape.intersects(w.getShape())) {
				this.x = -1000;
				this.y = -1000;
			}

			if (this.shape.intersects(w.getShape()) && w.getType() == "wood") {
				w.setX(-1000);
			}
		}
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		if (this.number == 1) {
			this.blueBullet.draw((int) this.x, (int) this.y);
		} else if (this.number == 2) {
			this.redBullet.draw((int) this.x, (int) this.y);
		}

		// Hitbox
		if (Game.SHOWHITBOX == true) {
			graphics.setColor(Color.orange);
			graphics.draw(this.shape);
			graphics.setColor(Color.white);
		}
	}

	public Shape getShape() {
		return shape;
	}

}
