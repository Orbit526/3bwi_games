package at.peter.game;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.SpriteSheet;

public class Details implements Actor {
	private String type;
	private double x, y;
	private Shape shape;
	private SpriteSheet grassSheet;
	private SpriteSheet torchSheet;
	private Image bushImage;
	
	private Animation grassAnimation;
	private Animation torchAnimation;

	public Details(String type, double x, double y) {
		super();
		this.type = type;
		this.x = x;
		this.y = y;

		// SpriteSheet sheet;
		try {
			this.grassSheet = new SpriteSheet("images/grassSheet.png", 32, 32);
			this.torchSheet = new SpriteSheet("images/torchSheet.png", 96, 96);
			this.bushImage = new Image("images/bush.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.grassAnimation = new Animation();
		this.torchAnimation = new Animation();

		for (int i = 0; i < 4; i++) {
			this.grassAnimation.addFrame(this.grassSheet.getSprite(i, 0), 400);
		}
		for (int i = 0; i < 4; i++) {
			this.torchAnimation.addFrame(this.torchSheet.getSprite(i, 0), 150);
		}

		this.shape = new Rectangle((int) this.x, (int) this.y, Game.TILESIZE, Game.TILESIZE);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		if (this.type == "grass") {
			this.grassAnimation.draw((int) this.x, (int) this.y);
		} else if (this.type == "torch") {
			this.torchAnimation.draw((int) this.x - Game.TILESIZE, (int) this.y - Game.TILESIZE);
		} else if (this.type == "bush") {
			this.bushImage.draw((int) this.x, (int) this.y);
		}
	}

}
