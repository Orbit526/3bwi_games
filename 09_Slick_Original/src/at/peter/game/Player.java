package at.peter.game;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {

	private int number, bullets;
	private double x, y, width, height, speed, damage, health;
	private boolean left, right, up, down;
	private Shape shape;

	private SpriteSheet playerSheet;
	private Image p1UpImage, p1DownImage, p1LeftImage, p1RightImage;
	private Image p2UpImage, p2DownImage, p2LeftImage, p2RightImage;

	public ArrayList<Wall> walls;

	public Player(int number, double x, double y, double width, double height, double speed, double damage,
			double health, int bullets) {
		super();
		this.number = number;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.speed = speed;
		this.damage = damage;
		this.health = health;
		this.bullets = bullets;

		try {
			this.playerSheet = new SpriteSheet("images/playerSheet.png", 32, 32);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Images for direction
		this.p1UpImage = this.playerSheet.getSprite(0, 0);
		this.p1DownImage = this.playerSheet.getSprite(1, 0);
		this.p1LeftImage = this.playerSheet.getSprite(2, 0);
		this.p1RightImage = this.playerSheet.getSprite(3, 0);

		this.p2UpImage = this.playerSheet.getSprite(0, 1);
		this.p2DownImage = this.playerSheet.getSprite(1, 1);
		this.p2LeftImage = this.playerSheet.getSprite(2, 1);
		this.p2RightImage = this.playerSheet.getSprite(3, 1);

		this.shape = new Rectangle((int) this.x, (int) this.y, (int) this.width, (int) this.height);
		this.walls = new ArrayList<>();
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub

		if (this.health == 0) {
			this.x = -1000;
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);
		}
		
		if(this.health > 100)
		{
			this.health = 100;
		}

		shape.setX((int) this.x);
		shape.setY((int) this.y);
	}

	public void move(int dx, int dy) {
		if (!collideWithWall(dx, dy)) {
			this.x += dx;
			this.y += dy; 
		}
	}

	public boolean collideWithWall(int dx, int dy) {
		for (Wall w : walls) {
			if (w.getX() == this.x + dx && w.getY() == this.y + dy) {
				return true;
			}
		}
		return false;
	}

	public void addWall(Wall w) {
		this.walls.add(w);
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		if (this.number == 1) {
			if (this.up == true) {
				this.p1UpImage.draw((int) this.x, (int) this.y);
			}
			if (this.down == true) {
				this.p1DownImage.draw((int) this.x, (int) this.y);
			}
			if (this.left == true) {
				this.p1LeftImage.draw((int) this.x, (int) this.y);
			}
			if (this.right == true) {
				this.p1RightImage.draw((int) this.x, (int) this.y);
			}
		} else if (this.number == 2) {
			if (this.up == true) {
				this.p2UpImage.draw((int) this.x, (int) this.y);
			}
			if (this.down == true) {
				this.p2DownImage.draw((int) this.x, (int) this.y);
			}
			if (this.left == true) {
				this.p2LeftImage.draw((int) this.x, (int) this.y);
			}
			if (this.right == true) {
				this.p2RightImage.draw((int) this.x, (int) this.y);
			}
		}

		// Hitbox
		if (Game.SHOWHITBOX == true) {
			graphics.setColor(Color.orange);
			graphics.draw(this.shape);
			graphics.setColor(Color.white);
		}
	}

	public int getBullets() {
		return bullets;
	}

	public void setBullets(int bullets) {
		this.bullets = bullets;
	}

	public Shape getShape() {
		return shape;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public boolean isUp() {
		return up;
	}

	public void setUp(boolean up) {
		this.up = up;
	}

	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public boolean isLeft() {
		return left;
	}

	public void setLeft(boolean left) {
		this.left = left;
	}

	public boolean isRight() {
		return right;
	}

	public void setRight(boolean right) {
		this.right = right;
	}

	public double getHealth() {
		return health;
	}

	public void setHealth(double health) {
		this.health = health;
	}

}
