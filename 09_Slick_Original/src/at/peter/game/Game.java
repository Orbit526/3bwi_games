package at.peter.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Game extends BasicGame {

	public static final int WIDTH = 960, HEIGHT = 768, FPS = 500, TILESIZE = 32;
	public static boolean SHOWHITBOX = false, SHOWHELP = false, GAMERUNNING = false;

	private List<Actor> actors;
	private List<Bullet> bullets;

	public static Player player1;
	public static Player player2;

	private Image backgroundGrass;
	private Image backgroundStone;
	private Image blueFlag;
	private Image redFlag;

	private Image startScreen;
	private Image endScreenP1;
	private Image endScreenP2;

	private SpriteSheet backgroundSheet;
	private Direction playerDirection;

	public enum Direction {
		UP, LEFT, DOWN, RIGHT
	};

	// 30x 24

	// 0 = Grass
	// 1 = Stonetiles

	private int[][] ground1 = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, };

	// 1 = Solid Brickwall
	// 2 = Unsolid Grass
	// 3 = Unsolid Torch
	// 4 = Unsolid Bush
	// 5 = Solid Woodwall
	// 6 = Healing

	private int[][] map1 = {
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 4, 2, 2, 1, 2, 2, 4, 2, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 2, 0, 2, 2, 2, 2, 1, 2, 2, 2, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5, 5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1 },
			{ 1, 0, 0, 0, 2, 2, 2, 1, 2, 2, 2, 0, 0, 0, 0, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 0, 0, 1 },
			{ 1, 0, 0, 0, 4, 2, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 1, 2, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 2, 0, 0, 2, 1, 0, 4, 3, 0, 2, 0, 2, 0, 0, 2, 0, 6, 2, 2, 1, 2, 0, 3, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 2, 0, 0, 1 },
			{ 1, 0, 0, 2, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 5, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 5, 0, 4, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 4, 5, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 5, 2, 0, 0, 0, 2, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 2, 5, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 1, 2, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 3, 0, 2, 1, 2, 2, 6, 0, 2, 0, 0, 0, 0, 0, 0, 3, 0, 2, 1, 2, 2, 0, 0, 0, 0, 1 },
			{ 1, 0, 2, 0, 2, 0, 0, 1, 2, 4, 2, 0, 1, 2, 0, 0, 0, 0, 2, 2, 4, 2, 1, 2, 0, 2, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 4, 0, 1, 2, 0, 2, 2, 1, 0, 0, 0, 0, 2, 0, 2, 2, 2, 1, 0, 2, 4, 2, 0, 0, 1 },
			{ 1, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5, 5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 2, 2, 2, 1, 2, 4, 2, 2, 2, 0, 0, 0, 0, 2, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 4, 2, 2, 1, 2, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 4, 2, 2, 4, 2, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

	private int[][] map2 = {
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, };

	public Game(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		// Draw the starting Screen
		showStartScreen();

		if (GAMERUNNING == true) {
			// Draw the Ground
			drawGround();

			// Draw the Flags
			this.blueFlag.draw(3 * TILESIZE, 2 * TILESIZE);
			this.redFlag.draw(26 * TILESIZE, 19 * TILESIZE);

			// Draw the Actors
			for (Actor actor : this.actors) {
				actor.render(graphics);
			}

			// Draw Health
			drawHealth(graphics);

			// Draw Help Sign
			graphics.drawString("Press F1 for Help", 100, 10);

			// Draw the amount of Bullets a Player has

			// Show Help
			if (SHOWHELP == true) {
				graphics.drawString(
						"\n Move Player1 with W, A, S, D \n Move Player2 with UP, DOWN, LEFT, RIGHT \n Player1 shoots with SPACE \n Player2 shoots with ENTER \n Press F2 to show Hitboxes",
						0, 30);
			}
		}

		if (player1.getHealth() == 0) {
			showEndScreen(2);
		} else if (player2.getHealth() == 0) {
			showEndScreen(1);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.actors = new ArrayList<>();
		this.bullets = new ArrayList<>();

		this.backgroundSheet = new SpriteSheet("images/sheet.png", 32, 32);
		this.backgroundGrass = this.backgroundSheet.getSprite(0, 0);
		this.backgroundStone = this.backgroundSheet.getSprite(1, 0);

		this.blueFlag = new Image("images/blueFlag.png");
		this.redFlag = new Image("images/redFlag.png");

		this.startScreen = new Image("images/startScreen.png");
		this.endScreenP1 = new Image("images/endScreenP1.png");
		this.endScreenP2 = new Image("images/endScreenP2.png");

		player1 = new Player(1, 96, 96, TILESIZE, TILESIZE, 0.2, 1, 100, 2000);
		player2 = new Player(2, 832, 640, TILESIZE, TILESIZE, 0.2, 1, 100, 2000);

		player1.setRight(true);
		player2.setLeft(true);

		// Draw the Walls
		drawTileObjects();

		this.actors.add(player1);
		this.actors.add(player2);
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}

		System.out.println("P1: " + player1.getBullets());
		System.out.println("P2: " + player2.getBullets());

		if (this.bullets.size() == 3) {
			this.bullets.remove(0);
		}

		// player1.setBullets(player1.getBullets() + 1);
		// player2.setBullets(player2.getBullets() + 1);

		System.out.println(this.bullets);
	}

	private void showStartScreen() {
		startScreen.draw();
	}

	private void showEndScreen(int player) {
		if (player == 1) {
			endScreenP1.draw();
		} else if (player == 2) {
			endScreenP2.draw();
		}
	}

	private void drawGround() {
		for (int row = 0; row < ground1.length; row++) {
			for (int col = 0; col < ground1[row].length; col++) {
				if (ground1[row][col] == 0) {
					this.backgroundGrass.draw((int) col * TILESIZE, (int) row * TILESIZE);
				}
				if (ground1[row][col] == 1) {
					this.backgroundStone.draw((int) col * TILESIZE, (int) row * TILESIZE);
				}
			}
		}
	}

	private void drawTileObjects() {

		for (int row = 0; row < map1.length; row++) {
			for (int col = 0; col < map1[row].length; col++) {
				if (map1[row][col] == 1) {

					Wall w = new Wall("brick", col * TILESIZE, row * TILESIZE, TILESIZE, TILESIZE);
					this.actors.add(w);
					player1.addWall(w);
					player2.addWall(w);
				}
				if (map1[row][col] == 2) {

					Details d = new Details("grass", col * TILESIZE, row * TILESIZE);
					this.actors.add(d);
				}
				if (map1[row][col] == 3) {

					Details d = new Details("torch", col * TILESIZE, row * TILESIZE);
					this.actors.add(d);
				}
				if (map1[row][col] == 4) {

					Details d = new Details("bush", col * TILESIZE, row * TILESIZE);
					this.actors.add(d);
				}
				if (map1[row][col] == 5) {

					Wall w = new Wall("wood", col * Game.TILESIZE, row * Game.TILESIZE, 32, 32);
					this.actors.add(w);
					player1.addWall(w);
					player2.addWall(w);
				}
				if (map1[row][col] == 6) {

					Healing h = new Healing(col * Game.TILESIZE, row * Game.TILESIZE, true);
					this.actors.add(h);
				}
			}
		}
	}

	

	private void drawHealth(Graphics graphics) {
		graphics.setColor(Color.blue);
		graphics.drawRect((float) player1.getX() - 9, (float) player1.getY() - 15, 50, 10);
		graphics.setColor(Color.green);
		graphics.fillRect((float) player1.getX() - 8, (float) player1.getY() - 14,
				(float) (player1.getHealth() / 2) - 1, 9);
		graphics.setColor(Color.red);
		graphics.drawRect((float) player2.getX() - 9, (float) player2.getY() - 15, 50, 10);
		graphics.setColor(Color.green);
		graphics.fillRect((float) player2.getX() - 8, (float) player2.getY() - 14,
				(float) (player2.getHealth() / 2) - 1, 9);
		graphics.setColor(Color.white);
	}

	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);

		if (GAMERUNNING == true) {
			// Movement player1
			if (key == Input.KEY_W) {
				player1.move(0, -TILESIZE);
				player1.setUp(true);
				player1.setDown(false);
				player1.setLeft(false);
				player1.setRight(false);
			}
			if (key == Input.KEY_S) {
				player1.move(0, TILESIZE);
				player1.setDown(true);
				player1.setUp(false);
				player1.setLeft(false);
				player1.setRight(false);
			}
			if (key == Input.KEY_A) {
				player1.move(-TILESIZE, 0);
				player1.setLeft(true);
				player1.setUp(false);
				player1.setDown(false);
				player1.setRight(false);
			}
			if (key == Input.KEY_D) {
				player1.move(TILESIZE, 0);
				player1.setRight(true);
				player1.setUp(false);
				player1.setDown(false);
				player1.setLeft(false);
			}

			// Movement player2
			if (key == Input.KEY_UP) {
				player2.move(0, -TILESIZE);
				player2.setUp(true);
				player2.setDown(false);
				player2.setLeft(false);
				player2.setRight(false);
			}
			if (key == Input.KEY_DOWN) {
				player2.move(0, TILESIZE);
				player2.setDown(true);
				player2.setUp(false);
				player2.setLeft(false);
				player2.setRight(false);
			}
			if (key == Input.KEY_LEFT) {
				player2.move(-TILESIZE, 0);
				player2.setLeft(true);
				player2.setUp(false);
				player2.setDown(false);
				player2.setRight(false);
			}
			if (key == Input.KEY_RIGHT) {
				player2.move(TILESIZE, 0);
				player2.setRight(true);
				player2.setUp(false);
				player2.setDown(false);
				player2.setLeft(false);
			}

			// Shooting player1
			if (key == Input.KEY_ENTER) {
				spawnBullet(1);
			}

			// Shooting player2
			if (key == Input.KEY_SPACE) {
				spawnBullet(2);
			}

			// Show Hitbox
			if (key == Input.KEY_F2 && SHOWHITBOX == false) {
				SHOWHITBOX = true;
			} else if (key == Input.KEY_F2 && SHOWHITBOX == true) {
				SHOWHITBOX = false;
			}

			// Show Hitbox
			if (key == Input.KEY_F1 && SHOWHELP == false) {
				SHOWHELP = true;
			} else if (key == Input.KEY_F1 && SHOWHELP == true) {
				SHOWHELP = false;
			}
		}

		// Key to Start the Game
		if (key == Input.KEY_ENTER && GAMERUNNING == false) {
			GAMERUNNING = true;
		}

		// Key to End the Game
		if (key == Input.KEY_ESCAPE) {
			System.exit(0);
		}
	}

	public void spawnBullet(int number) {
		// Player1 shooting
		if (number == 1 && player1.getBullets() != 0) {
			if (player1.isUp() == true) {
				Bullet bullet = new Bullet(1, player1.getX(), player1.getY(), 1, Direction.UP);
				actors.add(bullet);
				bullets.add(bullet);
				player1.setBullets(player1.getBullets() - 1);
			}
			if (player1.isDown() == true) {
				Bullet bullet = new Bullet(1, player1.getX(), player1.getY(), 1, Direction.DOWN);
				actors.add(bullet);
				bullets.add(bullet);
				player1.setBullets(player1.getBullets() - 1);
			}
			if (player1.isLeft() == true) {
				Bullet bullet = new Bullet(1, player1.getX(), player1.getY(), 1, Direction.LEFT);
				actors.add(bullet);
				bullets.add(bullet);
				player1.setBullets(player1.getBullets() - 1);
			}
			if (player1.isRight() == true) {
				Bullet bullet = new Bullet(1, player1.getX(), player1.getY(), 1, Direction.RIGHT);
				actors.add(bullet);
				bullets.add(bullet);
				player1.setBullets(player1.getBullets() - 1);
			}
		} else if (number == 2 && player2.getBullets() != 0) {
			// Player2 shooting
			if (player2.isUp() == true) {
				Actor bullet = new Bullet(2, player2.getX(), player2.getY(), 1, Game.Direction.UP);
				actors.add(bullet);
				player2.setBullets(player2.getBullets() - 1);
			}
			if (player2.isDown() == true) {
				Actor bullet = new Bullet(2, player2.getX(), player2.getY(), 1, Game.Direction.DOWN);
				actors.add(bullet);
				player2.setBullets(player2.getBullets() - 1);
			}
			if (player2.isLeft() == true) {
				Actor bullet = new Bullet(2, player2.getX(), player2.getY(), 1, Game.Direction.LEFT);
				actors.add(bullet);
				player2.setBullets(player2.getBullets() - 1);
			}
			if (player2.isRight() == true) {
				Actor bullet = new Bullet(2, player2.getX(), player2.getY(), 1, Game.Direction.RIGHT);
				actors.add(bullet);
				player2.setBullets(player2.getBullets() - 1);
			}
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Game("Mighty Bullets"));
			container.setDisplayMode(WIDTH, HEIGHT, false);
			// container.setTargetFrameRate(FPS);
			container.start();

		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
