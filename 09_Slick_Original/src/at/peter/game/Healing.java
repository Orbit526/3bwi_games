package at.peter.game;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Healing implements Actor {

	private double x, y;
	private boolean hasHealth;

	private Shape shape;

	private SpriteSheet healingSheet;
	private Image healingHealthImage, healingNoHealthImage;

	public Healing(double x, double y, boolean hasHealth) {
		super();
		this.x = x;
		this.y = y;
		this.hasHealth = hasHealth;

		try {
			this.healingSheet = new SpriteSheet("images/healingSheet.png", 32, 32);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.shape = new Rectangle((int) this.x, (int) this.y, (int) Game.TILESIZE, (int) Game.TILESIZE);

		// Images for direction
		this.healingHealthImage = this.healingSheet.getSprite(0, 0);
		this.healingNoHealthImage = this.healingSheet.getSprite(1, 0);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if (this.shape.intersects(Game.player1.getShape()) && this.hasHealth == true
				&& Game.player1.getHealth() != 100) {
			Game.player1.setHealth(Game.player1.getHealth() + 50);
			this.hasHealth = false;
		}
		if (this.shape.intersects(Game.player2.getShape()) && this.hasHealth == true
				&& Game.player2.getHealth() != 100) {
			Game.player2.setHealth(Game.player2.getHealth() + 50);
			this.hasHealth = false;
		}
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		if (hasHealth == true) {
			this.healingHealthImage.draw((int) this.x, (int) this.y);
		} else if (hasHealth == false) {
			this.healingNoHealthImage.draw((int) this.x, (int) this.y);
		}

		if (Game.SHOWHITBOX == true) {
			graphics.setColor(Color.orange);
			graphics.draw(shape);
			graphics.setColor(Color.white);
		}
	}

}
