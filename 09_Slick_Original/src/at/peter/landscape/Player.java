package at.peter.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Player implements Actor {

	private double x, y, speed;
	private Image image;

	public Player(double x, double y, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;

		try {
			this.image = new Image("images/player.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			this.y -= delta * this.speed;
		} else if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			this.y += delta * this.speed;
		}
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(Color.orange);
		// graphics.fillRect((int) this.x, (int) this.y, 40, 40);
		graphics.setColor(Color.white);

		this.image.draw((int) this.x, (int) this.y, 64, 90);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
