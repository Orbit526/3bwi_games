package at.peter.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class HTLOval implements Actor {

	private double x, y;
	private int width, height;
	private double speed;
	private Shape shape;
	private List<Shape> collisonPartners;

	public HTLOval(double x, double y, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.speed = speed;

		this.collisonPartners = new ArrayList<>();
		this.shape = new Circle((int) this.x, (int) this.y, 50);
	}

	public void update(GameContainer gc, int delta) {
		this.y += delta * this.speed;

		if (this.y >= 600) {
			this.y = 0;
		}

		shape.setX((float) this.x);
		shape.setY((float) this.y);

		for (Shape s : collisonPartners) {
			if (s.intersects(this.shape)) {
				System.out.println("Collision");
			}
		}

	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.height);
		graphics.setColor(Color.white);
		graphics.draw(shape);
	}

	public Shape getShape() {
		return shape;
	}

	public void setCollisonPartners(Shape shape) {
		this.collisonPartners.add(shape);
	}
}
