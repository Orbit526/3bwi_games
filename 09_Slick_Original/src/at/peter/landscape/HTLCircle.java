package at.peter.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class HTLCircle implements Actor {

	private double x, y;
	boolean left = false, right = true;
	private int width, height;
	private double speed;
	private Shape shape;

	public HTLCircle(double x, double y, boolean left, boolean right, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.left = left;
		this.right = right;
		this.width = width;
		this.height = height;
		this.speed = speed;

		this.shape = new Circle((int) this.x, (int) this.y, 50);
	}

	public void update(GameContainer gc, int delta) {
		// Kreis hin und her
		if (this.x >= 600) {
			this.left = true;
			this.right = false;
		} else if (this.x <= 50) {
			this.left = false;
			this.right = true;
		}

		if (this.left == true) {
			this.x -= delta * this.speed;
		} else if (this.right == true) {
			this.x += delta * this.speed;
		}

		shape.setX((float) this.x);
		shape.setY((float) this.y);
	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.blue);
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.height);
		graphics.setColor(Color.white);

		graphics.draw(shape);
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}
}
