package at.peter.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Landscape extends BasicGame {

	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	public boolean gameStopped = false;

	private List<Actor> actors;

	private Player player1;

	public Landscape() {
		super("Lanscape");
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// Render wird nur aufgerufen wenn es ben�tigt wird.
		for (Actor actor : this.actors) {
			actor.render(graphics);
		}

		graphics.drawString("\n Press ESC to stop game \n Press UP and DOWN to move Player \n Press SPACE to shoot", 0,
				30);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// Wird ein mal aufgerufen
		this.actors = new ArrayList<>();
		this.actors.add(new HTLRectangle(100, 100, 0, 100, 100, 0.001));
		this.actors.add(new HTLRectangle(100, 100, 0, 100, 100, 0.002));
		this.actors.add(new HTLRectangle(100, 100, 0, 100, 100, 0.003));
		this.actors.add(new HTLRectangle(100, 100, 0, 100, 100, 0.004));
		// this.actors.add(new HTLCircle(50, 200, false, true, 100, 100, 0.4));

		// Snowfalkes
		for (int i = 0; i <= 100; i++) {
			this.actors.add(new Snowflakes(7, 7, 0.1));
			this.actors.add(new Snowflakes(12, 12, 0.2));
			this.actors.add(new Snowflakes(25, 25, 0.3));
		}

		HTLOval o1 = new HTLOval(400, 30, 100, 50, 0.4);
		HTLCircle c1 = new HTLCircle(50, 200, false, true, 100, 100, 0.4);

		this.actors.add(o1);
		this.actors.add(c1);

		o1.setCollisonPartners(c1.getShape());

		player1 = new Player(40, 400, 0.5);
		this.actors.add(player1);

	}

	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if (key == Input.KEY_SPACE) {
			Actor bullet = new Bullet(this.player1.getX(), this.player1.getY(), 0.7);
			actors.add(bullet);
		}
		if (key == Input.KEY_ESCAPE) {
			if (gameStopped == false) {
				this.gameStopped = true;
			} else {
				this.gameStopped = false;
			}
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// Delta ist die Zeit im ms seit dem letzten Aufruf

		if (gameStopped == false) {
			for (Actor actor : this.actors) {
				actor.update(gc, delta);
			}
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(WIDTH, HEIGHT, false);
			container.setTargetFrameRate(500);
			container.start();

		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
