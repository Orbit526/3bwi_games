package at.peter.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLRectangle implements Actor {

	private double x, y, degree;
	private int width, height;
	private double speed;

	public HTLRectangle(double x, double y, double degree, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.degree = degree;
		this.width = width;
		this.height = height;
		this.speed = speed;
	}

	public void update(GameContainer gc, int delta) {
		// (int)(startposition + radius * Math.sin(this.degreeX));
		this.x = (int) (200 + 200 * Math.sin(this.degree += delta * this.speed));
		this.y = (int) (200 + 200 * Math.cos(this.degree += delta * this.speed));
	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.red);
		graphics.drawRect((int) this.x, (int) this.y, this.width, this.height);
		graphics.setColor(Color.white);
	}

}
