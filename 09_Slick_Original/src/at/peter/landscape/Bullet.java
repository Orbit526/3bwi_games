package at.peter.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Bullet implements Actor {
	private double x, y, speed;
	private Image image;

	public Bullet(double x, double y, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		try {
			this.image = new Image("images/root2.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		this.x += delta * this.speed;
	}

	@Override
	public void render(Graphics graphics) {
		this.image.draw((int) this.x, (int) this.y);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}
}
