package at.peter.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Snowflakes implements Actor {

	private double x, y;
	private int width, height;
	private double speed;
	private Image image;

	public Snowflakes(int width, int height, double speed) {
		super();
		this.x = coordinateX();
		this.y = coordinateY();
		this.width = width;
		this.height = height;
		this.speed = speed;

		try {
			this.image = new Image("images/snowflake.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		this.y += delta * this.speed;

		if (this.y >= Landscape.HEIGHT) {
			this.x = coordinateX();
			this.y = coordinateY();
		}

	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(Color.white);
		this.image.draw((int) this.x, (int) this.y, this.width, this.height);
		// graphics.fillOval((int) this.x, (int) this.y, this.width, this.height);
		graphics.setColor(Color.white);

	}

	private double coordinateX() {
		double x = (int) (Math.random() * (Landscape.WIDTH - 0)) + 0;
		return x;
	}

	private double coordinateY() {
		double y = (int) (Math.random() * (-Landscape.HEIGHT - 0)) + 0;
		return y;
	}

}
