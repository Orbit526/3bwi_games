package at.peter.chessproblem;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Queen implements Actor {

	private double x, y;
	private boolean isColliding;

	public Queen(double x, double y, boolean isColliding) {
		super();
		this.x = x;
		this.y = y;
		this.isColliding = isColliding;
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(Color.orange);
		graphics.fillRect((float) this.x, (float) this.y, Chess.TILESIZE, Chess.TILESIZE);
		graphics.setColor(Color.blue);
		graphics.fillRect((float) this.x + 10, (float) this.y + 10, Chess.TILESIZE - 20, Chess.TILESIZE - 20);
		graphics.setColor(Color.white);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
