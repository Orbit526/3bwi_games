package at.peter.chessproblem;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.peter.chessproblem.Actor;

public class Chess extends BasicGame {

	private List<Actor> actors;
	private List<Queen> queens;

	public static final int TILESIZE = 64, WIDTH = 800, HEIGHT = 800;

	public static Queen q1, q2;

	private int[][] board1 = { { 0, 0, 0, 0, 0, 0, 0, 0, }, { 0, 0, 0, 0, 0, 0, 0, 0, }, { 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, }, { 0, 0, 0, 0, 0, 0, 0, 0, }, { 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, }, { 0, 0, 0, 0, 0, 0, 0, 0, }, };

	public Chess(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub

		// Draw Grid
		for (int i = 0; i <= 8; i++) {
			graphics.drawLine(i * TILESIZE, 0, i * TILESIZE, HEIGHT);
		}
		for (int i = 0; i <= 8; i++) {
			graphics.drawLine(0, i * TILESIZE, WIDTH, i * TILESIZE);
		}

		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		this.actors = new ArrayList<>();
		this.queens = new ArrayList<>();

		q1 = new Queen(3 * TILESIZE, 3 * TILESIZE, false);
		q2 = new Queen(2 * TILESIZE, 0 * TILESIZE, false);

		this.queens.add(q1);
		this.queens.add(q2);

		this.actors.add(q1);
		this.actors.add(q2);

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}

		for (int i = 1; i < 9; i++) {
			if (q1.getX() == q2.getX() - 1 * TILESIZE || q1.getX() == q2.getX() + 1 * TILESIZE) {
				System.out.println("collision!");
			}
		}

//		for (Queen q : queens)
//		{
//			if(q.getX() == q.getX())
//			{
//				System.out.println("ajegl");
//			}
//		}

	}

//	private boolean isColliding()
//	{
//		
//	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Chess("Chess Problem"));
			container.setDisplayMode(800, 800, false);
			container.start();

		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
